#!/bin/bash
# Just a simple script to remember to install all the lua rocks that we need to work confortable
#
# Author MrTimeout
#
arr=("dkjson htmlparser luasocket lunajson luasec")

for x in ${arr[@]}; do
  luarocks install $x
done
