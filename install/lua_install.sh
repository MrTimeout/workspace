#/bin/bash
# Install lua from source
#
# By default, it will take the last version.
#
# Author: MrTimeout
#
LUA_URL=http://www.lua.org/ftp/

LAST_VERSION_TAR_GZ=$(curl -X GET -sSfL $LUA_URL | sed -n 's/.*\(lua\-[0-9\.]\+\.tar\.gz\).*/\1/p' | head -n 1)
LAST_VERSION_CHECKSUM=$(curl -X GET -sSfL $LUA_URL | sed -n 's/.*\([0-9a-f]\{64\}\).*/\1/p' | head -n 1)

OUTPUT_DIRECTORY_TAR_GZ=/tmp/$LAST_VERSION_TAR_GZ
LAST_VERSION=${LAST_VERSION_TAR_GZ%\.tar\.gz}
OUTPUT_DIRECTORY=/tmp/$LAST_VERSION

function clean_up {
  echo $OUTPUT_DIRECTORY_TAR_GZ
  test -f $OUTPUT_DIRECTORY_TAR_GZ && rm -rf $OUTPUT_DIRECTORY_TAR_GZ
  test -d $OUTPUT_DIRECTORY && rm -rf $OUTPUT_DIRECTORY
}

clean_up

curl -X GET -sSfL ${LUA_URL}$LAST_VERSION_TAR_GZ -o $OUTPUT_DIRECTORY_TAR_GZ && \
  test $(sha256sum $OUTPUT_DIRECTORY_TAR_GZ | cut -d ' ' -f1) == $LAST_VERSION_CHECKSUM && \
  tar -xvzf $OUTPUT_DIRECTORY_TAR_GZ -C /tmp/ && \
  cd $OUTPUT_DIRECTORY && make linux test && make install

clean_up
