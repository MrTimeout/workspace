#!/bin/bash
#
# Basic script to install the latest version of the Go programming language
#
# Author: MrTimeout
#
# dependencies
pip3 install lxml requests

URL=https://go.dev/dl/
LAST_VERSION=$(curl -X GET -sSfL $URL | sed -n 's/.*\(go[0-9\.]\+linux\-amd64\.tar\.gz\).*/\1/p' | head -n 1)
LAST_VERSION_NUMBER=${LAST_VERSION%\.tar\.gz}

echo "We are going to install the last version of go: $LAST_VERSION_NUMBER"

sha256=$(python3 <<HEREDOC
from lxml import html
import requests

response = requests.get('$URL')
h = html.fromstring(response.text)
e = h.xpath("//tr//a[contains(@href,'$LAST_VERSION_NUMBER')]/../../td/tt")
print(e[0].text_content())
HEREDOC
)

function clean {
  test -f /tmp/$LAST_VERSION && rm -rf /tmp/$LAST_VERSION
}

clean

curl -X GET -sSfL ${URL}${LAST_VERSION} -o /tmp/$LAST_VERSION && \
  test $(sha256sum /tmp/$LAST_VERSION | cut -d ' ' -f1) == $sha256 && \
  rm -rf /usr/local/go && \
  tar -xvf /tmp/$LAST_VERSION -C /usr/local && \
  echo "export PATH=\$PATH:/usr/local/go/bin" >> ~/.bashrc && source ~/.bashrc && go version

clean
