#/bin/bash
# Install luarocks automatically using this script.
# This script will install the last version of the control of dependencies.
# Also remember that this script don't uninstall previous luarocks library. It trusts in lua makefile.
#
# Author: MrTimeout
#
# Where is the GPG Key: https://pgp.mit.edu/pks/lookup?op=get&search=0x3FD8F43C2BB3C478
# Github issue related to it: https://github.com/luarocks/luarocks/issues/643

gpg --import ./luarocks.pub
LUAROCKS_URL="https://luarocks.github.io/luarocks/releases/"
LAST_VERSION_TAR_GZ=$(curl -X GET -sSfL $LUAROCKS_URL | sed -n 's/.*\(luarocks\-[0-9\.]\+\.tar\.gz\).*/\1/p' | head -n 1)

LAST_VERSION=${LAST_VERSION_TAR_GZ%\.tar\.gz}
OUTPUT_DIRECTORY_TAR_GZ=/tmp/$LAST_VERSION_TAR_GZ
OUTPUT_DIRECTORY=/tmp/$LAST_VERSION

test -d $OUTPUT_DIRECTORY_TAR_GZ && rm -rf $OUTPUT_DIRECTORY_TAR_GZ
test -d $OUTPUT_DIRECTORY && rm -rf $OUTPUT_DIRECTORY

curl -X GET -sSfL "${LUAROCKS_URL}${LAST_VERSION_TAR_GZ}" -o $OUTPUT_DIRECTORY_TAR_GZ && \
  curl -X GET -sSfL "${LUAROCKS_URL}${LAST_VERSION_TAR_GZ}.asc" -o "${OUTPUT_DIRECTORY_TAR_GZ}.asc" && \
  gpg --verify ${OUTPUT_DIRECTORY_TAR_GZ}.asc && \
  tar -xvzf $OUTPUT_DIRECTORY_TAR_GZ -C /tmp/ && \
  cd $OUTPUT_DIRECTORY && ./configure && make && make install && luarocks --version
