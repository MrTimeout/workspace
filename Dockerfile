FROM Ubuntu:22.04

# build-essential and libreadline-dev is used for building lua from sources.
# libssl-dev is used to get the headers of openssl, so it allows to lua to install luasec (for instance).
# libxml2-utils are utils for parsing xml and html files. Use xmllint.
# apt-file is an awesome command that will list all the files that are inside a deb file.
RUN apt-get update && apt-get install --yes curl wget git jq neovim python3 python3-pip build-essential libreadline-dev unzip libssl-dev libxml2-utils apt-file
